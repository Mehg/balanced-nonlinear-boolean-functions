package hr.fer.zavrsni.fitness;

import hr.fer.zavrsni.BooleanFunction;

public class NonLinearityFitness implements IFitness {
    public Double maxNonLinearity;

    @Override
    public double calculateFitness(BooleanFunction function) {
        if (maxNonLinearity == null) {
            int noVars = function.getNumberOfVariables();
            maxNonLinearity = Math.pow(2, noVars - 1) - Math.pow(2, noVars / 2.0 - 1);
        }
        return function.getNonLinearity();
    }

    @Override
    public boolean reachedMax(BooleanFunction function) {
        return function.getNonLinearity()==maxNonLinearity;
    }

    public Double getMaxNonLinearity() {
        return maxNonLinearity;
    }
}
