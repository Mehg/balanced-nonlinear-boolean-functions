package hr.fer.zavrsni.fitness;

import hr.fer.zavrsni.BooleanFunction;

import java.util.List;

public class BalancedFitness implements IFitness {
    @Override
    public double calculateFitness(BooleanFunction function) {
        List<Integer> truthTable = function.getTruthTable();
        int ones = truthTable.stream().mapToInt(r->r).sum();
        int zeros = truthTable.size() - ones;
        if(ones == zeros)
            return 1;
        return 1.0/Math.abs(ones-zeros);
    }

    @Override
    public boolean reachedMax(BooleanFunction function) {
        return function.isBalanced();
    }
}
