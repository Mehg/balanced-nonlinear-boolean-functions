package hr.fer.zavrsni.fitness;

import hr.fer.zavrsni.BooleanFunction;

import java.util.Arrays;
import java.util.List;

public class CompositeFitness implements IFitness {
    private final List<IFitness> fitnesses;

    public CompositeFitness(IFitness... fitnesses) {
        this.fitnesses = Arrays.asList(fitnesses);
    }

    @Override
    public double calculateFitness(BooleanFunction function) {
        double fitness = 0;
        for (IFitness iFitness : fitnesses)
            fitness += iFitness.calculateFitness(function);
        return fitness;

    }

    @Override
    public boolean reachedMax(BooleanFunction function) {
        for (IFitness iFitness : fitnesses) {
            if (!iFitness.reachedMax(function))
                return false;
        }
        return true;
    }
}
