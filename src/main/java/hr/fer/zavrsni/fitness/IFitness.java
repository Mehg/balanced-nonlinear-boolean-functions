package hr.fer.zavrsni.fitness;

import hr.fer.zavrsni.BooleanFunction;

public interface IFitness {

    double calculateFitness(BooleanFunction function);
    boolean reachedMax(BooleanFunction function);
}
