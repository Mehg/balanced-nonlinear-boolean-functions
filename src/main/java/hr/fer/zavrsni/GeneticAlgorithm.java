package hr.fer.zavrsni;

import hr.fer.zavrsni.fitness.BalancedFitness;
import hr.fer.zavrsni.fitness.CompositeFitness;
import hr.fer.zavrsni.fitness.IFitness;
import hr.fer.zavrsni.fitness.NonLinearityFitness;
import hr.fer.zavrsni.mutation.ExactlyOneMutation;
import hr.fer.zavrsni.mutation.IMutation;
import hr.fer.zavrsni.mutation.RateForBitMutation;
import hr.fer.zavrsni.selection.ISelection;
import hr.fer.zavrsni.selection.RouletteWheelSelection;
import hr.fer.zavrsni.selection.TournamentSelection;

import java.sql.Time;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;

public class GeneticAlgorithm {
    private final int NO_VARS = 8;
    private final int POPULATION_SIZE = 5000;
    private final int MAX_GEN = 1000;
    private final double MUTATION_RATE = 0.1;
    private final Random RANDOM = new Random();

    private IFitness fitness;
    private ISelection selection;
    private IMutation mutation;
    private List<BooleanFunction> population;
    private List<BooleanFunction> newPopulation;

    public GeneticAlgorithm(IFitness fitness, ISelection selection, IMutation mutation) {
        this.fitness = fitness;
        this.selection = selection;
        this.mutation = mutation;
        generateRandomPopulation();
    }

    private void generateRandomPopulation() {
        population = new ArrayList<>(POPULATION_SIZE);
        for (int i = 0; i < POPULATION_SIZE; i++) {
            BooleanFunction function = BooleanFunction.generateRandom(NO_VARS);
            function.setFitness(fitness.calculateFitness(function));
            population.add(function);
        }
        population.sort(Comparator.comparing(BooleanFunction::getFitness).reversed());
    }

    public BooleanFunction runAlgorithm() {
        for (int i = 0; i < MAX_GEN; i++) {
            newPopulation = new ArrayList<>();
            newPopulation.add(population.get(0));

            while (newPopulation.size() < POPULATION_SIZE) {
                BooleanFunction parent1 = selection.select(population, fitness);
                BooleanFunction parent2 = selection.select(population, fitness);
                BooleanFunction child = reproduction(parent1, parent2);
                child.setFitness(fitness.calculateFitness(child));

                if (fitness.reachedMax(child))
                    return child;

                newPopulation.add(child);
            }

            population = newPopulation;
            population.sort(Comparator.comparing(BooleanFunction::getFitness).reversed());
        }
        return population.get(0);
    }


    private BooleanFunction reproduction(BooleanFunction parent1, BooleanFunction parent2) {
        List<Integer> childTable = crossover(parent1, parent2);
        mutation.mutate(childTable);
        return new BooleanFunction(parent1.getNumberOfVariables(), childTable);
    }

    private List<Integer> crossover(BooleanFunction parent1, BooleanFunction parent2) {
        List<Integer> truthTable1 = parent1.getTruthTable();
        List<Integer> truthTable2 = parent2.getTruthTable();
        List<Integer> childTable = new ArrayList<>();

        int n = truthTable1.size();
        int index = RANDOM.nextInt(n);

        for (int i = 0; i < n; i++) {
            if (i < index)
                childTable.add(truthTable1.get(i));
            else
                childTable.add(truthTable2.get(i));
        }

        return childTable;
    }


    public static void main(String[] args) {
        IFitness fitness = new CompositeFitness(new NonLinearityFitness(), new BalancedFitness());
        ISelection selection = new TournamentSelection();
        IMutation mutation = new ExactlyOneMutation();

        GeneticAlgorithm algorithm = new GeneticAlgorithm(fitness, selection, mutation);
        long start = System.currentTimeMillis();
        BooleanFunction result = algorithm.runAlgorithm();
        long end = System.currentTimeMillis();

        System.out.println("Fitness: " + result.getFitness());
        System.out.println("Nonlinearity: " + result.getNonLinearity());
        System.out.println("Balanced: " + result.isBalanced());
        System.out.println("Truth table: " + result);
        System.out.println("Time: " + (end - start) / 1000.0 + " s");
    }
}
