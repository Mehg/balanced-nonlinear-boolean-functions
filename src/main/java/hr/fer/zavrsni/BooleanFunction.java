package hr.fer.zavrsni;

import java.util.*;

public class BooleanFunction {
    private final int numberOfVariables;
    private final List<Integer> truthTable;
    private final boolean balanced;
    private final FastWalshTransform walshTransform;
    private final double nonLinearity;
    private double fitness;

    public BooleanFunction(int numberOfVariables, List<Integer> truthTable) {
        this.numberOfVariables = numberOfVariables;
        this.truthTable = truthTable;
        this.walshTransform = new FastWalshTransform(this);
        this.balanced = calculateBalanced();
        this.nonLinearity = calculateNonLinearity();
    }

    public static BooleanFunction generateRandom(int numberOfVariables) {
        Random random = new Random();
        int n = (int) Math.pow(2, numberOfVariables);
        List<Integer> truthTable = new ArrayList<>(n);

        for (int i = 0; i < n; i++) {
            truthTable.add(random.nextInt(2));
        }
        return new BooleanFunction(numberOfVariables, truthTable);
    }

    private double calculateNonLinearity() {
        if (walshTransform == null)
            throw new IllegalArgumentException("No walsh transform found");

        OptionalInt maxCoefficient = Arrays.stream(walshTransform.getWalshCoefficients())
                .map(Math::abs).max();

        if (maxCoefficient.isEmpty())
            throw new IllegalArgumentException("Couldn't find max coefficient");

        return Math.pow(2, numberOfVariables - 1) - maxCoefficient.getAsInt() / 2.0;
    }

    private boolean calculateBalanced(){
        int ones = truthTable.stream().mapToInt(r->r).sum();
        int zeros = truthTable.size() - ones;
        return ones==zeros;
    }

    public int getNumberOfVariables() {
        return numberOfVariables;
    }

    public List<Integer> getTruthTable() {
        return truthTable;
    }

    public double getNonLinearity() {
        return nonLinearity;
    }

    public boolean isBalanced() {
        return balanced;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Integer i : truthTable)
            sb.append(i);
        return sb.toString();
    }
}
