package hr.fer.zavrsni.mutation;

import java.util.List;

public interface IMutation {
    void mutate(List<Integer> truthTable);
}
