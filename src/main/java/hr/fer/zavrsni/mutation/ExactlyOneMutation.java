package hr.fer.zavrsni.mutation;

import java.util.List;
import java.util.Random;

public class ExactlyOneMutation implements IMutation {
    @Override
    public void mutate(List<Integer> truthTable) {
        Random rnd = new Random();
        int index = rnd.nextInt(truthTable.size());
        truthTable.set(index, 1 - truthTable.get(index));
    }
}
