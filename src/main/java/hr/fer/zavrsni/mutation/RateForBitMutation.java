package hr.fer.zavrsni.mutation;

import java.util.List;

public class RateForBitMutation implements IMutation {
    private double mutationRate;

    public RateForBitMutation(double mutationRate) {
        this.mutationRate = mutationRate;
    }

    @Override
    public void mutate(List<Integer> truthTable) {
        int n = truthTable.size();

        for (int i = 0; i < n; i++) {
            if (shouldMutate()) {
                int value = truthTable.get(i);
                truthTable.set(i, 1 - value);
            }
        }
    }

    private boolean shouldMutate() {
        return Math.random() <= mutationRate;
    }

}
