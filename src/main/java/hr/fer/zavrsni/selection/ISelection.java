package hr.fer.zavrsni.selection;

import hr.fer.zavrsni.BooleanFunction;
import hr.fer.zavrsni.fitness.IFitness;

import java.util.List;

public interface ISelection {
    BooleanFunction select(List<BooleanFunction> population, IFitness fitness);
}
