package hr.fer.zavrsni.selection;

import hr.fer.zavrsni.BooleanFunction;
import hr.fer.zavrsni.fitness.IFitness;

import java.util.List;
import java.util.Random;

public class TournamentSelection implements ISelection {
    private final int TOURNAMENT_SIZE = 3;

    @Override
    public BooleanFunction select(List<BooleanFunction> population, IFitness fitness) {
        BooleanFunction winner = null;
        for (int i = 0; i < TOURNAMENT_SIZE; i++) {
            int index = new Random().nextInt(population.size());
            BooleanFunction function = population.get(index);

            if (winner == null || fitness.calculateFitness(function) >= fitness.calculateFitness(winner)) {
                winner = function;
            }
        }
        return winner;
    }
}
