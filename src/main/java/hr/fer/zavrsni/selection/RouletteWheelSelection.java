package hr.fer.zavrsni.selection;

import hr.fer.zavrsni.BooleanFunction;
import hr.fer.zavrsni.fitness.IFitness;

import java.util.List;

public class RouletteWheelSelection implements ISelection{
    @Override
    public BooleanFunction select(List<BooleanFunction> population, IFitness fitness) {
        double sum = population.stream().mapToDouble(fitness::calculateFitness).sum();
        double randomSum = Math.random() * sum;

        double current = 0;
        BooleanFunction parent = null;
        for (BooleanFunction function : population) {
            if (current > randomSum)
                break;
            parent = function;
            current += fitness.calculateFitness(function);
        }

        return parent;
    }
}
