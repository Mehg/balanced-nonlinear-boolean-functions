package hr.fer.zavrsni;

import java.util.Arrays;

public class FastWalshTransform {
    private int[] walshCoefficients;

    public FastWalshTransform(BooleanFunction function) {
        this.walshCoefficients = new int[function.getTruthTable().size()];

        int index = 0;
        for (Integer i : function.getTruthTable()) {
            walshCoefficients[index++] = i == 0 ? 1 : -1;
        }
        walshCoefficients = fwtStep(walshCoefficients);
    }

    private int[] fwtStep(int[] coefficients) {
        if (coefficients.length == 1)
            return coefficients;

        int[] firstGroup = Arrays.copyOfRange(coefficients, 0, coefficients.length / 2);
        int[] secondGroup = Arrays.copyOfRange(coefficients, coefficients.length / 2, coefficients.length);
        int[] firstResult = new int[firstGroup.length];
        int[] secondResult = new int[secondGroup.length];

        for (int i = 0; i < firstGroup.length; i++) {
            firstResult[i] = firstGroup[i] + secondGroup[i];
            secondResult[i] = firstGroup[i] - secondGroup[i];
        }

        int[] result = Arrays.copyOf(fwtStep(firstResult), firstResult.length + secondResult.length);
        System.arraycopy(fwtStep(secondResult), 0, result, firstResult.length, secondResult.length);
        return result;
    }

    public int[] getWalshCoefficients() {
        return walshCoefficients;
    }
}
